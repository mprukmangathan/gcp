terraform {
  backend "gcs" {
    bucket  = "terraform-state-file-7482"
    prefix  = "bitbucket/gcp"
  }
}

provider "google" {
  credentials = file("credentials.json")
  region      = "us-central1"
  project     = "dazzling-matrix-361211"
}
